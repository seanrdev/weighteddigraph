//
// Created by seanrdev on 11/14/17.
//
#include <iostream>
#include <vector>
#include <queue>
#include "Graph.h"
#include <iterator>
#include <algorithm>
#include <locale>
#include <bits/stdc++.h>
#include <unordered_map>
//using namespace std;

struct node{
    unsigned int vector;
    unsigned int weight;
};

Graph::Graph(unsigned int routes, unsigned int stations) {
    this->num_of_stations = stations;
    this->num_of_routes = routes;
    adjList = new vector<unsigned int>[stations];
    weights = new vector<unsigned int>[routes];
    //weight = new vector<node>[100]();
    unordered_map<unsigned int, string> *map = new unordered_map<unsigned int, string>();
    station_numbers = new vector<unsigned int>[100]();
    myList = new vector<node>();
    matrix[99][99] = {-1};
    data = std::vector<vector<node> >(100, vector<node>(100));
    adj = new vector<unsigned int>[100]();
}

void Graph::addRoute(unsigned int from, unsigned int to, unsigned int weight) {
    vector<unsigned int>::iterator it;
    this->station_numbers[from].push_back(to);
    //node a;
    //a.vector = to;
    //a.weight = weight;
    //this->weight[from].push_back(a);
    //bool f_exists, t_exists;
    //f_exists = false;
    //t_exists = false;
    //for(it = this->station_numbers->begin(); it != this->station_numbers->end(); ++it){
    //    if(from == *it){
    //        f_exists = true;
    //    }
    //    if(to == *it){
    //        t_exists = true;
    //    }
    //}
    //if(f_exists == false){
    //    this->station_numbers->push_back(from);
    //}
    //if(t_exists == false){
    //    this->station_numbers[to].push_back(to);
    //}
    std::cout << "Adding route From: "<< from << " To: " << to << std::endl;
    this->matrix[from][to] = weight;
    std::cout << "Stored: " << this->matrix[from][to] << std::endl;
}

bool Graph::isRoute(unsigned int from, unsigned int to) {
    /*
    vector<unsigned int> t = breadth(from);
    vector<unsigned int>::iterator iter;
    for(iter = t.begin(); iter != t.end(); ++iter){
        if(*iter == to){
            return true;
        }
    }
    return false;
     */
}

void Graph::breadth(unsigned int s){
    //cout << "This is a test: " << this->weight[0].at(0).weight << endl;
    list<unsigned int> *qu = new list<unsigned int>();
    bool visit[this->num_of_stations] = {false};
    visit[s] = true;
    qu->push_back(s);
    vector<unsigned int>::iterator it;
    while(!qu->empty()){
        s = qu->front();
        cout << s << endl;
        qu->pop_front();
        for(it = this->station_numbers[s].begin(); it != this->station_numbers[s].end(); ++it){
            //cout << *it << endl;
            if(!visit[*it]){
                visit[*it] = true;
                qu->push_back(*it);
            }
        }
    }
}

void Graph::calcShortestRoutes(void) {
    int i, j, k;
    for(k = 0; k < 100; k++){
        for(i = 0; i < 100; i++){
            for(j = 0; j < 100; j++){
                if(this->matrix[i][k] + this->matrix[k][j] < this->matrix[i][j]){
                    this->matrix[i][j] = this->matrix[i][k] + this->matrix[k][j];
                }
            }
        }
    }

}

int Graph::shortestRoute(string src, string dst) {
    this->calcShortestRoutes();
    unsigned int a, b;
    for(int i = 0; i < this->colors.size(); i++){
        if(this->colors.at(i).compare(src) == 0){
            a = this->num_rep.at(i);
        }
        if(this->colors.at(i).compare(dst) == 0){
            b = this->num_rep.at(i);
        }
    }
    return this->matrix[a][b];
}

void Graph::setMap(unordered_map<unsigned int, string> data){
    this->map = &data;
}

void Graph::indexStationNumber(unsigned int i) {
    this->station_numbers->push_back(i);
}

void Graph::vect_to_set(void){
    //this->unique_station_numbers = new set<unsigned int>(this->station_numbers->begin(), this->station_numbers->end());
}

void Graph::matrixCall(){
    for(int i = 0; i < 10; i++){
        for(int j = 0; j < 10; j++){
            cout << this->matrix[i][j] << " ";
        }
        cout << "\n";
    }
}

void Graph::initMatrix() {
    for(int i = 0; i < 100; i++){
        for(int j = 0; j < 100; j++){
            this->matrix[i][j] = 99999;
        }
    }
}

void Graph::passColor(string s) {
    string temp = s;
    transform(temp.begin(), temp.end(), temp.begin(), ::tolower);
    this->colors.push_back(temp);
}

void Graph::passNum(int i) {
    this->num_rep.push_back(i);
}

void Graph::addEdge(unsigned int a, unsigned int b) {
    this->adj[a].push_back(b);
}