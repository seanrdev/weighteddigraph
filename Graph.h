//
// Created by seanrdev on 11/14/17.
//

#ifndef GRAPHPROJECT_GRAPH_H
#define GRAPHPROJECT_GRAPH_H

#include "station.h"
#include <vector>
#include <unordered_map>
#include <string>
#include <set>
#include <array>

class Graph {
private:
    unsigned int num_of_stations; //amount of stations
    unsigned int num_of_routes;   //amount of routes
    //vector<node> *weight;
    vector<unsigned int> *adjList;
    vector<unsigned int> *adj; //edges
    //vector<std::unordered_map<unsigned int, unsigned int>> *myList;
    vector<struct node> *myList;
    vector<unsigned int> *weights;
    vector<unsigned int> *station_numbers;
    set<unsigned int> *unique_station_numbers;
    //unsigned int matrix[][stations];
    std::unordered_map<unsigned int, string> *map;
    vector<string> colors;
    vector<int> num_rep;
    //unordered_map<unsigned int, string> mapper = {};
    //std::vector<std::vector<node> > data;
    vector<vector<node> > data;
    vector<vector<unsigned int>> *n;
    int matrix[100][100];
public:
    void addEdge(unsigned int, unsigned int);
    Graph(unsigned int routes, unsigned int stations);
    void addRoute(unsigned int from, unsigned int to, unsigned int weight);
    bool isRoute(unsigned int from, unsigned int to);
    void breadth(unsigned int);
    void calcShortestRoutes(void);
    //void calcShortestRoutes(unsigned int matrix[][this->num_of_stations]);
    int shortestRoute(string src, string dst);
    void indexStationNumber(unsigned int);
    void vect_to_set(void);
    void initMatrix();
    void matrixCall();
    void setMap( std::unordered_map<unsigned int, std::string> data );
    void passColor(string);
    void passNum(int);
};


#endif //GRAPHPROJECT_GRAPH_H
