#include <iostream>
#include <fstream>
#include <unordered_map>
#include <string>
#include "Graph.h"
#include <regex>
#include <queue>
#include <stdlib.h>
#include <bits/unordered_map.h>

//#define string f1 = "input.txt", f2 = "input2.txt"

using namespace std;

unsigned int userData();

void checkPath(Graph *g);

int main() {
    const string f1 = "/home/seardev/input.txt", f2 = "/home/seardev/input2.txt";
    ifstream File_1, File_2;
    File_1.open(f1);
    File_2.open(f2);
    string input;
    string input2;
    unordered_map<unsigned int, string> mapper = {};
    bool first_line = true;
    Graph *g;
    bool isInit = false;
    while(getline(File_1, input)) {
        if(first_line == true){
            regex rgx("([0-9]+) ([0-9]+)");
            smatch matches;
            if(regex_search(input, matches, rgx)) {
                g = new Graph(stoi(matches[2].str()), stoi(matches[1].str()));
                if(isInit == false){
                    g->initMatrix();
                    isInit = true;
                }
                first_line = false;
                continue;
            }
            first_line = false;
        }
        regex rgx("([0-9]+) ([0-9]+) ([0-9]+)");
        smatch matches;
        if(regex_search(input, matches, rgx)) {
            g->addRoute(stoi(matches[1].str()), stoi(matches[2].str()), stoi(matches[3].str()));
            //g->indexStationNumber(stoul(matches[1].str()));
            //g->indexStationNumber(stoul(matches[2].str()));
            continue;
        }
    }
    g->vect_to_set();

    while(getline(File_2, input2)){
        regex rgx("([0-9]+) ([a-zA-Z]+)");
        smatch matches;
        if(regex_search(input2, matches, rgx)){
            //mapper.insert({stoul(matches[1].str()), matches[2].str()});
            //mapper stores 'station #' : 'Color'
            g->passNum(stoul(matches[1].str()));
            g->passColor(matches[2].str());
        }
    }
    bool contin = true;
    //g->setMap(mapper);
    while(contin == true){
        unsigned int option = userData();
        if(option == 0){
            contin = false;
            continue;
        }
        if(option == 1){
            g->breadth(9);
            g->matrixCall();
            continue;
        }
        if(option == 2){
            //Check to see if path exists.
            checkPath(g);
        }
        if(option == 3){
            g->matrixCall();
        }
    }
    return 0;
}

unsigned int userData(){
    char data;
    cout << "---------------+++++Menu+++++---------------" << endl;
    cout << "|  Selectable Options:                     |" << endl;
    cout << "|  E => Exit Program                       |" << endl;
    cout << "|  P => Print this menu                    |" << endl;
    cout << "|  C => Check if path exists               |" << endl;
    cout << "|  M => Print matrix                       |" << endl;
    cout << "|                                          |" << endl;
    cout << "|                                          |" << endl;
    cout << "---------------++++++++++++++---------------" << endl;
    cout << "%> ";
    cin >> data;
    if(data == 69 || data == 101){
        return 0;//e
    } else if(data == 80 || data == 112){
        return 1;//p
    } else if(data == 67 || data == 99){
        return 2; //c
    } else if(data == 77 || data == 109){//for debugging matrix
        return 3; //m
    } else {
        system("clear");
        cout << "Please select one of the appropriate characters from the menu." << endl;
        return 1;
    }
}

void checkPath(Graph *g){
    string source, destination;
    cout << "From what station are you leaving? ";
    cin >> source;
    cout << "\nTo what station are you traveling? ";
    cin >> destination;
    cout << "\n" << endl;
    string is_route = "There is a route between " + source + " and " + destination + " with a minimum distance of ";
    string miles = " miles.";
    string no_route = "There is no route between " + source + " and " + destination + ".";
    transform(source.begin(), source.end(), source.begin(), ::tolower);
    transform(destination.begin(), destination.end(), destination.begin(), ::tolower);
    int a = g->shortestRoute(source, destination);
    if(a != -1){
        system("clear");
        cout << "-------------------------------------------------------------------------------" << endl;
        cout << is_route << a << miles << endl;
        cout << "-------------------------------------------------------------------------------" << endl;
    }else{
        system("clear");
        cout << "-------------------------------------------------------------------------------" << endl;
        cout << no_route << endl;
        cout << "-------------------------------------------------------------------------------" << endl;
    }
}