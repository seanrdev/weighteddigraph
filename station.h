//
// Created by seanrdev on 11/16/17.
//

#ifndef GRAPHPROJECT_STATION_H
#define GRAPHPROJECT_STATION_H

#include <vector>

using namespace std;



class station {
private:
    //vector that holds
    //node address, route, node address
    vector<station*> to_from;
    vector<unsigned int> weight;
    unsigned int single_route;
    unsigned int station_id;
public:
    station(unsigned int);
    station* get_pointing_to(unsigned int index);
    unsigned int getWeight(unsigned int index);
    unsigned int getId();
    void add_pointing_to(station *);
    void addWeight(unsigned int);
    void increase_index();
    size_t amount_stations_pointed_to();
};


#endif //GRAPHPROJECT_STATION_H
